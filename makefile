.PHONY: clean

all: clean mylib scanner parser compile jar

mylib:	
	mkdir classes
	javac -cp lib/asm-all-5.1.jar:. src/*.java -d classes
	jar -cf lib/MyLib.jar -C classes .

scanner: 
	#jflex Scanner.jflex
	java -jar lib/jflex-1.6.1.jar Scanner.jflex

parser: 
	#cup -parser Parser Parser.cup
	#java -jar ./lib/java-cup-11b.jar -parser Parser Parser.cup
	#java -cp ./lib/MyLib.jar -jar ./lib/java-cup-11b.jar -parser Parser Parser.cup
	java -jar ./lib/java-cup-11b.jar -parser Parser Parser.cup

compile: 
	#javac -classpath ./lib/java-cup-11a-runtime.jar Parser.java Scanner.java sym.java
	javac -Xdiags:verbose -Xlint:deprecation -cp ./lib/java-cup-11b-runtime.jar:./lib/MyLib.jar:. Parser.java Scanner.java sym.java

jar:
	#java -jar Parser.class Scanner.class sym.class Compiler.jar
	#jar cf myJar.jar *.class
	jar -cvfm Compiler.jar manifest.txt *.class classes/*.class

clean:
	rm -f *.java
	rm -f *~
	rm -rdf classes
	rm -f *.class
	rm -f classes/*.class


