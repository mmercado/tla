import java.io.*;
import java_cup.runtime.SymbolFactory;
%%
%cup
%class Scanner
%{
	public Scanner(java.io.Reader r, SymbolFactory sf){
		this(r);
		this.sf=sf;
	}
	private SymbolFactory sf;
%}
%eofval{
    return sf.newSymbol("EOF",sym.EOF);
%eofval}

%%
";" { return sf.newSymbol("Semicolon",sym.SEMI); }
"+" { return sf.newSymbol("Plus",sym.PLUS); }
"-" { return sf.newSymbol("Minus",sym.MINUS); }
"*" { return sf.newSymbol("Times",sym.TIMES); }
"(" { return sf.newSymbol("Left Bracket",sym.LPAREN); }
")" { return sf.newSymbol("Right Bracket",sym.RPAREN); }
"{" { return sf.newSymbol("Lcur",sym.LCUR); }
"}" { return sf.newSymbol("Rcur",sym.RCUR); }
"." { return sf.newSymbol("Dot",sym.DOT); }
"," { return sf.newSymbol("Coma",sym.COMA); }
"\"" { return sf.newSymbol("DoubleQuote",sym.DQ); }
"\'" { return sf.newSymbol("SingleQuote",sym.SQ); }
"[" { return sf.newSymbol("Left Square",sym.LSQUARE); }
"]" { return sf.newSymbol("Right Square",sym.RSQUARE); }
"void" { return sf.newSymbol("Void",sym.VOID); }
"return" { return sf.newSymbol("Return",sym.RETURN); }
"int[]" { return sf.newSymbol("int",sym.INTARRAY); }
"String[]" { return sf.newSymbol("int",sym.STRINGARRAY); }
"double[]" { return sf.newSymbol("int",sym.DOUBLEARRAY); }
"boolean[]" { return sf.newSymbol("int",sym.BOOLEANARRAY); }
"int" { return sf.newSymbol("int",sym.INT); }
"double" { return sf.newSymbol("double",sym.DOUBLE); }
"boolean" { return sf.newSymbol("boolean",sym.BOOLEAN); }
"String" { return sf.newSymbol("String",sym.STRING); }
"/" { return sf.newSymbol("Divition",sym.DIV); }
"main" { return sf.newSymbol("Point",sym.MAIN); }
">" { return sf.newSymbol("Greater",sym.G); }
"<" { return sf.newSymbol("Lower",sym.L); }
">=" { return sf.newSymbol("GreaterEquals",sym.GE); }
"<=" { return sf.newSymbol("LowerEquals",sym.LE); }
"==" { return sf.newSymbol("Equals",sym.EQUALS); }
"!=" { return sf.newSymbol("NotEquals",sym.NOTEQUALS); }
"%" { return sf.newSymbol("Module",sym.MODULE); }
"&&" { return sf.newSymbol("And",sym.AND); }
"||" { return sf.newSymbol("Or",sym.OR); }
"!" { return sf.newSymbol("not",sym.NOT); }
"=" { return sf.newSymbol("Assingment",sym.ASSINGMENT); }
"if" { return sf.newSymbol("If",sym.IF); }
"while" { return sf.newSymbol("While",sym.WHILE); }
"true" { return sf.newSymbol("True",sym.TRUE, new Boolean(true)); }
"false" { return sf.newSymbol("False",sym.FALSE, new Boolean(false)); }
"else" { return sf.newSymbol("Else",sym.ELSE); }
"print" { return sf.newSymbol("Print",sym.PRINT); }
"readLine" { return sf.newSymbol("Readline",sym.READLINE); }
"readInt" { return sf.newSymbol("Readline",sym.READINT); }
"inReader();" { return sf.newSymbol("inReader",sym.INREADER); }
"d:" { return sf.newSymbol("DoubleIndicator", sym.DOUBLEINDICATOR); }
(0|-?[1-9][0-9]*)\.[0-9]+ { return sf.newSymbol("Doouble Number",sym.DOUBLENUMBER, new Double(yytext())); }
0|[1-9][0-9]* { return sf.newSymbol("Integer Number",sym.NUMBER, new Integer(yytext())); }
[a-z][a-z0-9A-Z]* { return sf.newSymbol("Name",sym.NAME, new String(yytext())); }
[A-Z]+ { return sf.newSymbol("Costant",sym.CONSTANT, new String(yytext())); }
\"[:jletterdigit:]*\" { return sf.newSymbol("Name",sym.WORD, new String(yytext())); }
[ \t\r\n\f] { /* ignore white space. */ }
. { System.err.println("Illegal character: "+yytext()); }
