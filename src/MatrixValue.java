
public class MatrixValue {
  private Matrix matrix;
  private IntegerExpresion fil;
  private IntegerExpresion col;

  public MatrixValue(Matrix matrix, IntegerExpresion fil, IntegerExpresion col) {
    this.matrix = matrix;
    this.fil = fil;
    this.col = col;
  }

  public Matrix getMatrix() {
    return matrix;
  }

  public void setMatrix(Matrix matrix) {
    this.matrix = matrix;
  }

  public IntegerExpresion getFil() {
    return fil;
  }

  public void setFil(IntegerExpresion fil) {
    this.fil = fil;
  }

  public IntegerExpresion getCol() {
    return col;
  }

  public void setCol(IntegerExpresion col) {
    this.col = col;
  }
}
