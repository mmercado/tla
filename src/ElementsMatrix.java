import java.util.List;

public class ElementsMatrix {
  int dim;
  int fillsAmount = 0;
  private List<Object> elements;

  public ElementsMatrix(List<Object> elements) {
    this.elements = elements;
    this.dim = elements.size();
  }

  public void addFil (List<Object> list) throws Exception {
    fillsAmount++;
    if(list.size() != this.dim) {
      throw new Exception("Error Columns Expected: "+ String.valueOf(this.dim));
    }
    elements.addAll(0,list);
  }

  public int getDim() {
    return dim;
  }

  public void setDim(int dim) {
    this.dim = dim;
  }

  public int getFillsAmount() {
    return fillsAmount;
  }

  public void setFillsAmount(int fillsAmount) {
    this.fillsAmount = fillsAmount;
  }

  public List<Object> getElements() {
    return elements;
  }

  public void setElements(List<Object> elements) {
    this.elements = elements;
  }
}
