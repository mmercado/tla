import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.commons.GeneratorAdapter;
import org.objectweb.asm.util.TraceClassVisitor;

public class Block {
  private DeclarationBlock declarationBlock;
  private CodeBlock codeBlock;

  public Block(DeclarationBlock declarationBlock, CodeBlock codeBlock) {
    this.declarationBlock = declarationBlock;
    this.codeBlock = codeBlock;
  }

  public DeclarationBlock getDeclarationBlock() {
    return declarationBlock;
  }

  public void setDeclarationBlock(DeclarationBlock declarationBlock) {
    this.declarationBlock = declarationBlock;
  }

  public CodeBlock getCodeBlock() {
    return codeBlock;
  }

  public void setCodeBlock(CodeBlock codeBlock) {
    this.codeBlock = codeBlock;
  }

  public void writeByteCode(TraceClassVisitor cw, GeneratorAdapter mg) {
    declarationBlock.writeByteCode(cw, mg);
    codeBlock.writeByteCode(cw, mg);

  }
}
