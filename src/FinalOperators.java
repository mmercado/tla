
public enum FinalOperators {

  INTEGER("Integer"), CALL("Call"), VARIABLE("Variable"), MATRIX("Matrix"), ARRAY("Array"), BOOLEAN ("Boolean"), DOUBLE ("Double"),
  STRING("String");
  String name;

  FinalOperators(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
