import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Type;
import org.objectweb.asm.commons.GeneratorAdapter;
import org.objectweb.asm.commons.Method;
import org.objectweb.asm.util.Textifier;
import org.objectweb.asm.util.TraceClassVisitor;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import static org.objectweb.asm.Opcodes.ACC_PUBLIC;
import static org.objectweb.asm.Opcodes.*;

public class Program {
    private static HashMap<String, FunctionPropereties> functionProperties = new HashMap<>();
    private static String currentFunction;
    private static Scanner scanner = new Scanner(System.in);
    private List<Function> list;
    private String name;
    private Main main;
    private List<FunctionDeclaration> imports;

    public Program (Main main, List<Function> functions, String name, List<FunctionDeclaration> imports) {
        this.list = functions;
        this.main = main;
        this.name = name;
        this.imports = imports;
    }

    public static Scanner getScanner() {
        return scanner;
    }

    public static void setScanner(Scanner scanner) {
        Program.scanner = scanner;
    }

    public List<FunctionDeclaration> getImports() {
        return imports;
    }

    public void setImports(List<FunctionDeclaration> imports) {
        this.imports = imports;
    }

    public static String getCurrentFunction() {
        return currentFunction;
    }

    public static void setCurrentFunction(String currentFunction) {
        Program.currentFunction = currentFunction;
    }

    public static HashMap<String, FunctionPropereties> getFunctionProperties() {
        return functionProperties;
    }

    public static void setFunctionProperties(HashMap<String, FunctionPropereties> functionProperties) {
        Program.functionProperties = functionProperties;
    }

    public void writeByteCode() throws IOException {
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);

        TraceClassVisitor cv = new TraceClassVisitor(cw, new Textifier(), new PrintWriter(System.out));

        cv.visit(V1_1, ACC_PUBLIC, "Example", null, "java/lang/Object", null);

        //

        //Initializes scanner
        FieldVisitor fv = cw.visitField(ACC_STATIC, "scanner", "Ljava/util/Scanner;", null, null);
        fv.visitEnd();

        // creates a GeneratorAdapter for the (implicit) constructor
        Method m = Method.getMethod("void <init> ()");
        GeneratorAdapter mg = new GeneratorAdapter(ACC_PUBLIC, m, null, null,
                cv);
        mg.loadThis();
        mg.invokeConstructor(Type.getType(Object.class), m);
        mg.returnValue();
        mg.visitMaxs(1,1);
        mg.endMethod();

        Program.setCurrentFunction("main");
        main.writeByteCode(cv);



        for(Function f: list){
            Program.setCurrentFunction(f.getName());
            f.writeByteCode(cv);
        }

        cv.visitEnd();

        byte[] code = cw.toByteArray();

        FileOutputStream fos = new FileOutputStream("Example.class");
        fos.write(code);
        fos.close();


    }

    public static void pushValue(GeneratorAdapter mv, Object value, Types type) {
        switch(type) {
            case INTEGEREXPRESION:
                mv.visitIntInsn(BIPUSH, (Integer)((FinalOperator)((IntegerExpresion)value).result()).getValue());
                mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
                break;

            case DOUBLEEXPRESION:
                mv.visitLdcInsn((Double)((FinalOperator)((DoubleExpresion)value).result()).getValue());
                mv.visitMethodInsn(INVOKESTATIC, "java/lang/Double", "valueOf", "(D)Ljava/lang/Double;", false);
                break;

            case STRING:
                String s = (String)value;
                mv.visitLdcInsn(s.substring(1, s.length()-1));
                break;

            case BOOLEANEXPRESION:
                Boolean val = (Boolean)((FinalOperator)((BooleanExpresion)value).result()).getValue();
                int op = val.equals(true) ? ICONST_1 : ICONST_0;
                mv.visitInsn(op);
                mv.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;", false);
                break;
        }
    }



    public List<Function> getList() {
        return list;
    }

    public void setList(List<Function> list) {
        this.list = list;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }
}
