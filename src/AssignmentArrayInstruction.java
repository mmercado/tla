import org.objectweb.asm.commons.GeneratorAdapter;
import org.objectweb.asm.util.TraceClassVisitor;

import static org.objectweb.asm.Opcodes.*;

public class AssignmentArrayInstruction implements Instructions {

  Object value;
  IntegerExpresion index;
  String nameVar;

  public AssignmentArrayInstruction(String nameVar, Object value, IntegerExpresion index) throws Exception {
    this.nameVar = nameVar;
    this.value = value;
    this.index = index;
    if(!check(nameVar, value, index)){
      throw new Exception("types Mismatched");
    }
  }

  private boolean check(String name, Object newValue, IntegerExpresion index) throws Exception {
    MyVariable currentVar = MyVariable.getByName(name);
    if(currentVar instanceof MyArray) {
      if(newValue instanceof FinalOperator) {
        switch (((FinalOperator) newValue).getType()) {
          case VARIABLE:
            MyVariable var = (MyVariable)((FinalOperator) newValue).getValue();
            if(!var.isInitialized()) {
              throw new Exception("Error variable "+ var.getName() +" in not initialized.");
            }
            if(var.getType().equals(((MyArray) currentVar).getElemTypes().getName())) {
              this.value = newValue;
              return true;
            } else {
              return false;
            }
          case ARRAY:
            ArrayValue arr = (ArrayValue)((FinalOperator) newValue).getValue();
            if(arr.getArray().getElemTypes().getName().equals(((MyArray) currentVar).getElemTypes().getName())) {
              this.value = newValue;
              return true;
            } else {
                return false;
            }
          case MATRIX:
            MatrixValue mat = (MatrixValue) ((FinalOperator) newValue).getValue();
            if(mat.getMatrix().getElementType().getName().equals(((MyArray) currentVar).getElemTypes().getName())) {
              this.value = newValue;
              return true;
            } else {
                return false;
            }
          case CALL:
            CallInstruction call = (CallInstruction) ((FinalOperator) newValue).getValue();
            if(FunctionDeclaration.getImports().get(call.getName()) == null) {
              return false;
            }
            if(!FunctionDeclaration.getImports().get(call.getName()).getType().getName().equals(currentVar.getType())){
              return false;
            }
            this.value = newValue;
            return true;
        }
      } else {

        if(!value.getClass().getSimpleName().equals(((MyArray) currentVar).getElemTypes().getName())){
          return false;
        }

      }
      this.value = newValue;
      return true;
    }
    return false;
  }

    @Override
    public TypeInstruction getType() {
        return TypeInstruction.ASSIGNMENTARRAY;
    }

    @Override
    public void writeByteCode(TraceClassVisitor cw, GeneratorAdapter mg) {
      MyArray arr = null;
      try {
        arr = (MyArray)MyVariable.getByName(this.nameVar);
      } catch (Exception e) {
        e.printStackTrace();
      }
        //Pushes the array reference
      mg.visitVarInsn(ALOAD, arr.getIndex());

        //Pushes the index
        index.writeByteCode(cw, mg);

        //Pushes the value to assign
        switch(arr.getElemTypes()) {
            case INTEGEREXPRESION:
                if( value instanceof IntegerExpresion) {
                    ((IntegerExpresion)value).writeByteCode(cw, mg);
                } else {
                    ((FinalOperator)value).writeByteCode(cw, mg);
                }
                break;
            case DOUBLEEXPRESION:
                if( value instanceof DoubleExpresion) {
                    ((DoubleExpresion)value).writeByteCode(cw, mg);
                } else {
                    ((FinalOperator)value).writeByteCode(cw, mg);
                }
                break;
            case BOOLEANEXPRESION:
                if( value instanceof BooleanExpresion) {
                    ((BooleanExpresion)value).writeByteCode(cw, mg);
                } else {
                    ((FinalOperator)value).writeByteCode(cw, mg);
                }
                break;
            case STRING:
                ((FinalOperator)value).writeByteCode(cw, mg);
                break;
            case ARRAY:
                ((FinalOperator)value).writeByteCode(cw, mg);
                break;
            case MATRIX:
                break;
        }

        //Asign the value
        mg.visitInsn(AASTORE);
    }
}
