import org.objectweb.asm.Type;
import org.objectweb.asm.commons.GeneratorAdapter;
import org.objectweb.asm.commons.Method;
import org.objectweb.asm.util.TraceClassVisitor;

import java.util.List;

import static org.objectweb.asm.Opcodes.ACC_PUBLIC;
import static org.objectweb.asm.Opcodes.ACC_STATIC;

public class Function {
  private Types type;
  private List<Args> args;
  private Block block;
  private String name;

  public Function(List<Args> args, Block block, Types type, String name) {
    this.args = args;
    this.type = type;
    this.name = name;
    this.block = block;
  }

  public Types getType() {
    return type;
  }

  public void setType(Types type) {
    this.type = type;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<Args> getArgs() {
    return args;
  }

  public void setArgs(List<Args> args) {
    this.args = args;
  }

  public Block getBlock() {
    return block;
  }

  public void setBlock(Block block) {
    this.block = block;
  }

  public void writeByteCode(TraceClassVisitor cw) {
    // creates a GeneratorAdapter for the 'main' method

    Type retType = null;
    Type[] types = null;

    //Method(String name, Type returnType, Type[] argumentTypes)
    //Allowed return types: Boolean, Double, Integer, Void

    if(type != Types.VOID) {
      retType = Type.getType(type.getClasz());
    }else{
      retType = Type.VOID_TYPE;
    }

    if(args!= null){
      int i=0;
      types = new Type[args.size()];
      for(Args arg: args){
        types[i] = Type.getType(arg.getType().getClasz());
        i++;
      }
    }else{
      types = new Type[]{};
    }


    Method m = new Method(name, retType, types);
    //Method m = Method.getMethod("void main (String[])");

    GeneratorAdapter mg = new GeneratorAdapter(ACC_PUBLIC + ACC_STATIC, m, null, null, cw);

    block.writeByteCode(cw, mg);
    mg.visitMaxs(1,1);
    mg.endMethod();
    return;
  }
}
