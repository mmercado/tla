
public class MyArray extends MyVariable{
  private int dim;
  private Types elemTypes = DeclarationBlock.getCurrentType();
  private Object[] element;

  public MyArray(String name, Object[] element) throws Exception{
    super(name);
    this.dim = element.length;
    this.element = element;
  }

  public MyArray(int dim, String name) throws Exception{
    super(name);
    this.dim = dim;
    this.element = new Object[dim];
  }

  public Types getElemTypes() {
    return elemTypes;
  }

  public void setElement(Object[] arr) {
    this.element = arr;
    this.dim = arr.length;
  }

  public Object getElem(IntegerExpresion index) {
    FinalOperator aux = (FinalOperator) index.result();
    if(aux != null){
      return this.element[(Integer)aux.getValue()];
    }
    else
    {
      return null;
    }
  }

  public void setElem(Integer index, Object value) throws Exception {
      this.element[index] = value;
    }


  public int getDim() {
    return dim;
  }

  @Override
  public String getType() {
    return Types.ARRAY.name;
  }

  @Override
  public void setValue(Object value) throws Exception {
    this.setElement((Object[])value);
  }

  public void setElemTypes(Types elemTypes) {
    this.elemTypes = elemTypes;
  }

  @Override
  public Object getValue() {
    return element;
  }

  public Types type(){
    return Types.ARRAY;
  }

}
