import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.commons.GeneratorAdapter;
import org.objectweb.asm.util.TraceClassVisitor;

public interface Instructions {

    public TypeInstruction getType();

    void writeByteCode(TraceClassVisitor cw, GeneratorAdapter mg);

}
