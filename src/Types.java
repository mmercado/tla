import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.commons.GeneratorAdapter;

public enum Types {
    EMPTYVARIABLE("EmptyVariable", EmptyVariable.class),CONSTANT("Constant", ConstantVariable.class),
    STRING ("String", String.class), ARRAY("Array", null), MATRIX("matrix", null),
    VOID("void", null), INTEGEREXPRESION("IntegerExpresion", Integer.class),
    BOOLEANEXPRESION("BooleanExpresion", Boolean.class), DOUBLEEXPRESION("DoubleExpresion", Double.class),
    INTARRAY("IntegerExpression[]", Integer[].class), BOOLEANARRAY("BooleanExpresion[]", Boolean[].class),
    DOUBLEARRAY("DoubleExpression[]", Double[].class),STRINGARRAY("StringExpression[]", String[].class);

    String name;
    Class clasz;

    Types(String string, Class clasz) {
        this.name = string;
        this.clasz = clasz;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Class getClasz(){
        return this.clasz;
    }

}
