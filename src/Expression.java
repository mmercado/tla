import org.objectweb.asm.commons.GeneratorAdapter;
import org.objectweb.asm.util.TraceClassVisitor;

public interface Expression {
    public void writeByteCode(TraceClassVisitor cw, GeneratorAdapter mg);

    public Object result();

}
