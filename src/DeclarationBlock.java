
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.commons.GeneratorAdapter;
import org.objectweb.asm.util.TraceClassVisitor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DeclarationBlock {
    public static List<MyVariable> allVariables = new ArrayList<>();

  private List<VariableBlock> variableBlockList;
    private static Types currentType;
    private static int variablesCount;

  public DeclarationBlock(List<VariableBlock> variableBlockList) {
    this.variableBlockList = variableBlockList;
  }


    public static Types getCurrentType() {
        return currentType;
    }

    public static void setCurrentType(Types currentType) {
        DeclarationBlock.currentType = currentType;
    }

    public List<VariableBlock> getVariableBlockList() {
        return variableBlockList;
    }

    public void setVariableBlockList(List<VariableBlock> variableBlockList) {
        this.variableBlockList = variableBlockList;
    }

    public static int getVariablesCount() {
        return variablesCount;
    }

    public static void setVariablesCount(int variablesCount) {
        DeclarationBlock.variablesCount = variablesCount;
    }

    public void writeByteCode(TraceClassVisitor cw, GeneratorAdapter mg) {
        int i = 1;

        for(VariableBlock vb:variableBlockList){

            i = vb.sortVariableList(i);
            vb.writeByteCode(cw, mg);
        }

        variablesCount = i;

    }
}
