import org.objectweb.asm.commons.GeneratorAdapter;
import org.objectweb.asm.util.TraceClassVisitor;

import java.util.List;

import static org.objectweb.asm.Opcodes.*;

public class CallInstruction implements Instructions{
    private TypeInstruction typeInstruction;
    private String name;
    private List<Object> args;

    public CallInstruction(String name, List<Object> args) throws Exception {
        this.name = name;
        this.args = args;
        this.typeInstruction = TypeInstruction.CALL;
        this.check(name, args);
    }

    private void check(String name, List<Object> args) throws Exception {
        FunctionDeclaration function = FunctionDeclaration.getImports().get(name);
        if( function == null) {
            throw new Exception("non declared function.");
        }
        if( (args==null && function.getArgs() != null) || (args!=null && function.getArgs() == null) ) {
            throw new Exception("non declared function.");
        }
        if(args == null) {
            return;
        }
        if(args.size() != function.getArgs().size()) {
            throw new Exception("non declared function.");
        }
        for(int i = 0; i < args.size() ; i++) {
            Types declaredType = function.getArgs().get(i).getType();
            Object current = args.get(i);
            if(current instanceof FinalOperator) {
                switch (((FinalOperator) current).getType()) {
                    case VARIABLE:
                        MyVariable var = (MyVariable)((FinalOperator) current).getValue();
                        if(var instanceof MyArray) {
                            switch (declaredType) {
                                case INTARRAY:
                                    if (!((MyArray) var).getElemTypes().equals(Types.INTEGEREXPRESION)) {
                                        throw new Exception("Invalid Argument, Expected Type: " + declaredType.getName());
                                    }
                                    break;
                                case DOUBLEARRAY:
                                    if (!((MyArray) var).getElemTypes().equals(Types.DOUBLEEXPRESION)) {
                                        throw new Exception("Invalid Argument, Expected Type: " + declaredType.getName());
                                    }
                                    break;
                                case BOOLEANARRAY:
                                    if (!((MyArray) var).getElemTypes().equals(Types.BOOLEANEXPRESION)) {
                                        throw new Exception("Invalid Argument, Expected Type: " + declaredType.getName());
                                    }
                                    break;
                                case STRINGARRAY:
                                    if (!((MyArray) var).getElemTypes().equals(Types.STRING)) {
                                        throw new Exception("Invalid Argument, Expected Type: " + declaredType.getName());
                                    }
                                    break;
                            }

                        }
                        if(!var.getType().equals(declaredType.getName())) {
                            throw new Exception("Invalid Argument, Expected Type: "+ declaredType.getName());
                        }
                        break;
                    case ARRAY:
                        ArrayValue arr = (ArrayValue)((FinalOperator) current).getValue();
                        if(!arr.getArray().getElemTypes().getName().equals(declaredType.getName())) {
                            throw new Exception("Invalid Argument, Expected Type: "+ declaredType.getName());
                        }
                    case MATRIX:
                        MatrixValue mat = (MatrixValue) ((FinalOperator) current).getValue();
                        if(!mat.getMatrix().getElementType().getName().equals(declaredType.getName())) {
                            throw new Exception("Invalid Argument, Expected Type: "+ declaredType.getName());
                        }
                        break;
                    case CALL:
                        CallInstruction call = (CallInstruction) ((FinalOperator) current).getValue();
                        if(FunctionDeclaration.getImports().get(call.getName()) == null) {
                            throw new Exception("Invalid Argument, Expected Type: "+ declaredType.getName());
                        }
                        if(!FunctionDeclaration.getImports().get(call.getName()).getType().equals(declaredType)){
                            throw new Exception("Invalid Argument, Expected Type: "+ declaredType.getName());
                        }
                        break;
                }
            } else {
                if(!current.getClass().getSimpleName().equals(declaredType.getName())){
                    throw new Exception("Invalid Argument, Expected Type: "+ declaredType.getName());
                }

            }
        }
    }

    public TypeInstruction getType() {
        return typeInstruction;
    }

    public void setTypeInstruction(TypeInstruction typeInstruction) {
        this.typeInstruction = typeInstruction;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Object> getArgs() {
        return args;
    }

    public void setArgs(List<Object> args) {
        this.args = args;
    }

    @Override
    public void writeByteCode(TraceClassVisitor cw, GeneratorAdapter mg) {
        FunctionDeclaration currentFunction = FunctionDeclaration.getImports().get(this.name);
        StringBuilder argsTypes = new StringBuilder("(");
        String template = "Ljava/lang/";

        //1. Pushes the args for the method called
        if(args != null) {
            Object arg;
            Types argType;
            for(int i=0; i<args.size(); i++) {
                arg = args.get(i);
                argType = currentFunction.getArgs().get(i).getType();
                Program.pushValue(mg, arg, argType);

                argsTypes.append(template);
                argsTypes.append(argType.getClasz().getSimpleName());
                argsTypes.append(";");
            }
        }
        argsTypes.append(")");

        Types returnedType = currentFunction.getType();
        if(returnedType == Types.VOID) {
            argsTypes.append("V");
        } else {
            argsTypes.append(template);
            argsTypes.append(returnedType.getClasz().getSimpleName());
            argsTypes.append(";");
        }
        mg.visitMethodInsn(INVOKESTATIC, "Example", this.name, argsTypes.toString(), false);
    }
}
