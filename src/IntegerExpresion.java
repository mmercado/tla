import org.objectweb.asm.commons.GeneratorAdapter;
import org.objectweb.asm.util.TraceClassVisitor;

import static org.objectweb.asm.Opcodes.*;
import static org.objectweb.asm.Opcodes.ASTORE;

public class IntegerExpresion implements Expression{
  private IntegerExpresion op1;
  private IntegerExpresion op2;
  private Operations operation;

  public IntegerExpresion(IntegerExpresion op1, IntegerExpresion op2, Operations operation) {
    this.op1 = op1;
    this.op2 = op2;
    this.operation = operation;
  }



  private FinalOperator value =  null;

  public IntegerExpresion(FinalOperator value) {
    this.value = value;
    this.operation = Operations.FINAL;
  }
    @Override
  public Object result() {
    if(value != null) {
      return this.value;
    }
    FinalOperator v1 = (FinalOperator) op1.result();
    FinalOperator v2 = (FinalOperator) op2.result();
    if(v1 != null && v2 != null) {
      if((v1.getValue() instanceof Integer)&&(v2.getValue() instanceof Integer)) {
        switch (operation) {
          case PLUS:
            this.setValue(new FinalOperator(FinalOperators.INTEGER, new Integer(((Integer) v1.getValue()).intValue() + ((Integer) v2.getValue()).intValue())));
            return this.value;
          case MINUS:
            this.setValue(new FinalOperator(FinalOperators.INTEGER, new Integer(((Integer) v1.getValue()).intValue() - ((Integer) v2.getValue()).intValue())));
            return this.value;
          case TIMES:
            this.setValue(new FinalOperator(FinalOperators.INTEGER, new Integer(((Integer) v1.getValue()).intValue() * ((Integer) v2.getValue()).intValue())));
            return this.value;
          case DIV:
            this.setValue(new FinalOperator(FinalOperators.INTEGER, new Integer(((Integer) v1.getValue()).intValue() / ((Integer) v2.getValue()).intValue())));
            return this.value;
          case MODULE:
            this.setValue(new FinalOperator(FinalOperators.INTEGER, new Integer(((Integer) v1.getValue()).intValue() % ((Integer) v2.getValue()).intValue())));
            return this.value;
        }
      }
    }
    return null;
  }

  public IntegerExpresion getOp1() {
    return op1;
  }

  public void setOp1(IntegerExpresion op1) {
    this.op1 = op1;
  }

  public IntegerExpresion getOp2() {
    return op2;
  }

  public void setOp2(IntegerExpresion op2) {
    this.op2 = op2;
  }

  public Operations getOperation() {
    return operation;
  }

  public void setOperation(Operations operation) {
    this.operation = operation;
  }

  public FinalOperator getValue() {
    return value;
  }

  public void setValue(FinalOperator value) {
    this.value = value;
  }

    @Override
    public void writeByteCode(TraceClassVisitor cw, GeneratorAdapter mg) {
        if(this.result() != null){
            FinalOperator f = (FinalOperator) result();
            f.writeByteCode(cw, mg);
        }else{
            op1.writeByteCode(cw, mg);
            op2.writeByteCode(cw, mg);
            switch (operation) {
                case PLUS:
                    mg.visitInsn(IADD);
                    mg.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
                    break;
                case MINUS:
                    mg.visitInsn(ISUB);
                    mg.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
                    break;
                case TIMES:
                    mg.visitInsn(IMUL);
                    mg.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
                    break;
                case DIV:
                    mg.visitInsn(IDIV);
                    mg.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
                    break;
                case MODULE:
                    mg.visitInsn(IREM);
                    mg.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
                    break;
            }
        }
    }
}
