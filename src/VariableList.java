import java.util.List;

public class VariableList {

  List<MyVariable> variables;

  public VariableList(List<MyVariable> variables) {
    this.variables = variables;
  }

  public List<MyVariable> getVariables() {
    return variables;
  }

  public void setVariables(List<MyVariable> variables) {
    this.variables = variables;
  }
}
