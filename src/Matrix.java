import java.util.ArrayList;
import java.util.List;

public class Matrix extends MyVariable{
  private int fils;
  private int cols;
  private Types elementType = DeclarationBlock.getCurrentType();
  private Object[] matrix;


  public Matrix(int fils, Object[] matrix,String name) throws Exception {
    super(name);
    this.fils = fils;
    this.matrix = matrix;
  }

  public Matrix(int fils, int cols, String name) throws Exception {
    super(name);
    this.fils = fils;
    this.cols = cols;
    this.matrix = new Object[fils * cols];
  }

  public String getType() {
    return Types.MATRIX.name;
  }

  @Override
  public void setValue(Object value) throws Exception {
    this.setMatrix((Object[])value);
  }

  public int getFils() {
    return fils;
  }

  public void setFils(int fils) {
    this.fils = fils;
  }

  public int getCols() {
    return cols;
  }

  public void setCols(int cols) {
    this.cols = cols;
  }

  public Types getElementType() {
    return elementType;
  }


  public Object[] getMatrix() {
    return matrix;
  }

  public void setMatrix(Object[] matrix) {
    this.matrix = matrix;
  }

  public Object getelem(IntegerExpresion fil, IntegerExpresion col) {
    FinalOperator p1 = (FinalOperator) fil.result();
    FinalOperator p2 = (FinalOperator) col.result();
    if(fil.result() != null && col.result() != null) {
      return this.matrix[((Integer) p1.getValue())*((Integer)p2.getValue())];
    } else {
      return null;
    }
  }


  public void setElem(IntegerExpresion fil, IntegerExpresion col, Object value) throws Exception {

      if(!this.elementType.name.equals(value.getClass().getSimpleName())) {
        throw new Exception("Matrix: Type Mismatched");
      }
        FinalOperator p1 = (FinalOperator) fil.result();
        FinalOperator p2 = (FinalOperator) col.result();
        if(p1 != null && p2 != null) {
          this.matrix[((Integer)p1.getValue()) * ((Integer)p2.getValue()) ] = value;
        }
  }

  @Override
  public Object getValue() {
    return this.matrix;
  }

  public Types type(){
    return Types.MATRIX;
  }
}
