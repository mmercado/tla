import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.*;

import org.objectweb.asm.*;
import org.objectweb.asm.util.Textifier;
import org.objectweb.asm.util.TraceClassVisitor;

import static org.objectweb.asm.Opcodes.*;

public class Test {

    static Scanner scanner;

    public static void main(String[] args) {
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void transformThis() throws Exception {

        Integer a = 1;

        while((a < 10)){
            a++;
        }


    }

     public static byte[] show(Integer a1, Double b1, Boolean c1,  Integer[] arr, Double[] arr2, Boolean[] arr3) throws Exception {

         ClassWriter cw = new ClassWriter(0);

         TraceClassVisitor cv = new TraceClassVisitor(cw, new Textifier(), new PrintWriter(System.out));

         FieldVisitor fv;
         MethodVisitor mv;
         AnnotationVisitor av0;

         cv.visit(52, ACC_PUBLIC + ACC_SUPER, "Test", null, "java/lang/Object", null);

         cv.visitSource("Test.java", null);

         {
             mv = cv.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
             mv.visitCode();
             Label l0 = new Label();
             mv.visitLabel(l0);
             mv.visitLineNumber(4, l0);
             mv.visitVarInsn(ALOAD, 0);
             mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
             mv.visitInsn(RETURN);
             Label l1 = new Label();
             mv.visitLabel(l1);
             mv.visitLocalVariable("this", "LTest;", null, l0, l1, 0);
             mv.visitMaxs(1, 1);
             mv.visitEnd();
         }
         {
             mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, "transformThis", "()V", null, new String[]{"java/lang/Exception"});
             mv.visitCode();
             Label l0 = new Label();
             mv.visitLabel(l0);
             mv.visitLineNumber(31, l0);
             mv.visitInsn(ICONST_1);
             mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
             mv.visitVarInsn(ASTORE, 0);
             Label l1 = new Label();
             mv.visitLabel(l1);
             mv.visitLineNumber(32, l1);
             mv.visitLdcInsn(new Double("2.0"));
             mv.visitMethodInsn(INVOKESTATIC, "java/lang/Double", "valueOf", "(D)Ljava/lang/Double;", false);
             mv.visitVarInsn(ASTORE, 1);
             Label l2 = new Label();
             mv.visitLabel(l2);
             mv.visitLineNumber(33, l2);
             mv.visitInsn(ICONST_1);
             mv.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;", false);
             mv.visitVarInsn(ASTORE, 2);
             Label l3 = new Label();
             mv.visitLabel(l3);
             mv.visitLineNumber(34, l3);
             mv.visitInsn(ICONST_2);
             mv.visitTypeInsn(ANEWARRAY, "java/lang/Integer");
             mv.visitInsn(DUP);
             mv.visitInsn(ICONST_0);
             mv.visitInsn(ICONST_1);
             mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
             mv.visitInsn(AASTORE);
             mv.visitInsn(DUP);
             mv.visitInsn(ICONST_1);
             mv.visitInsn(ICONST_2);
             mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
             mv.visitInsn(AASTORE);
             mv.visitVarInsn(ASTORE, 3);
             Label l4 = new Label();
             mv.visitLabel(l4);
             mv.visitLineNumber(35, l4);
             mv.visitIntInsn(BIPUSH, 10);
             mv.visitTypeInsn(ANEWARRAY, "java/lang/Double");
             mv.visitVarInsn(ASTORE, 4);
             Label l5 = new Label();
             mv.visitLabel(l5);
             mv.visitLineNumber(36, l5);
             mv.visitInsn(ICONST_2);
             mv.visitTypeInsn(ANEWARRAY, "java/lang/Boolean");
             mv.visitInsn(DUP);
             mv.visitInsn(ICONST_0);
             mv.visitInsn(ICONST_1);
             mv.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;", false);
             mv.visitInsn(AASTORE);
             mv.visitInsn(DUP);
             mv.visitInsn(ICONST_1);
             mv.visitVarInsn(ALOAD, 2);
             mv.visitInsn(AASTORE);
             mv.visitVarInsn(ASTORE, 5);
             Label l6 = new Label();
             mv.visitLabel(l6);
             mv.visitLineNumber(38, l6);
             mv.visitInsn(ICONST_1);
             mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
             mv.visitLdcInsn(new Double("2.0"));
             mv.visitMethodInsn(INVOKESTATIC, "java/lang/Double", "valueOf", "(D)Ljava/lang/Double;", false);
             mv.visitInsn(ICONST_1);
             mv.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;", false);
             mv.visitIntInsn(BIPUSH, 10);
             mv.visitTypeInsn(ANEWARRAY, "java/lang/Integer");
             mv.visitIntInsn(BIPUSH, 10);
             mv.visitTypeInsn(ANEWARRAY, "java/lang/Double");
             mv.visitIntInsn(BIPUSH, 10);
             mv.visitTypeInsn(ANEWARRAY, "java/lang/Boolean");
             mv.visitMethodInsn(INVOKESTATIC, "Test", "show", "(Ljava/lang/Integer;Ljava/lang/Double;Ljava/lang/Boolean;[Ljava/lang/Integer;[Ljava/lang/Double;[Ljava/lang/Boolean;)[B", false);
             mv.visitInsn(POP);
             Label l7 = new Label();
             mv.visitLabel(l7);
             mv.visitLineNumber(40, l7);
             mv.visitVarInsn(ALOAD, 0);
             mv.visitVarInsn(ALOAD, 1);
             mv.visitVarInsn(ALOAD, 2);
             mv.visitVarInsn(ALOAD, 3);
             mv.visitVarInsn(ALOAD, 4);
             mv.visitVarInsn(ALOAD, 5);
             mv.visitMethodInsn(INVOKESTATIC, "Test", "show", "(Ljava/lang/Integer;Ljava/lang/Double;Ljava/lang/Boolean;[Ljava/lang/Integer;[Ljava/lang/Double;[Ljava/lang/Boolean;)[B", false);
             mv.visitInsn(POP);
             Label l8 = new Label();
             mv.visitLabel(l8);
             mv.visitLineNumber(41, l8);
             mv.visitInsn(RETURN);
             Label l9 = new Label();
             mv.visitLabel(l9);
             mv.visitLocalVariable("a", "Ljava/lang/Integer;", null, l1, l9, 0);
             mv.visitLocalVariable("b", "Ljava/lang/Double;", null, l2, l9, 1);
             mv.visitLocalVariable("c", "Ljava/lang/Boolean;", null, l3, l9, 2);
             mv.visitLocalVariable("a1", "[Ljava/lang/Integer;", null, l4, l9, 3);
             mv.visitLocalVariable("a2", "[Ljava/lang/Double;", null, l5, l9, 4);
             mv.visitLocalVariable("a3", "[Ljava/lang/Boolean;", null, l6, l9, 5);
             mv.visitMaxs(6, 6);
             mv.visitEnd();
         }
         cv.visitEnd();
         //cv.p.print(System.out);
         //System.out.println(cw.toByteArray().toString());
         return cw.toByteArray();
     }


}
