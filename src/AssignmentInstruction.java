
import org.objectweb.asm.commons.GeneratorAdapter;
import org.objectweb.asm.util.TraceClassVisitor;


public class AssignmentInstruction implements Instructions{

    TypeInstruction type;
    String name;
    Object value;

  public AssignmentInstruction(String name, Object value) throws Exception{
    this.type = TypeInstruction.ASSIGNMENT;
    this.name = name;
    this.value = value;
    if(!check(name, value)) {
      throw new Exception("AssigmentInstruction: Type Mismatched");
    }
  }

  private boolean check(String name, Object newValue) throws Exception {
    MyVariable currentVar = MyVariable.getByName(name);
    if(currentVar instanceof ConstantVariable) {
      throw new Exception("Error "+ currentVar.getName() + "is contant.");
    }
    if(newValue instanceof FinalOperator) {
      switch (((FinalOperator) newValue).getType()) {
        case VARIABLE:
          MyVariable var = (MyVariable)((FinalOperator) newValue).getValue();
          if(!var.isInitialized()) {
            throw new Exception("Error variable "+ var.getName() +" in not initialized.");
          }

          if(var.getType().equals(currentVar.getType())) {
            if(var instanceof MyArray && currentVar instanceof MyArray){
              if(((MyArray) var).getElemTypes().equals(currentVar.getTypes())){
                return true;
              } else {
                return false;
              }
            }
          //  currentVar.setValue(newValue);
            this.value = newValue;
            return true;
          } else {
            return false;
          }

        case ARRAY:
          ArrayValue arr = (ArrayValue)((FinalOperator) newValue).getValue();
          if(arr.getArray().getElemTypes().getName().equals(currentVar.getType())) {
        //    currentVar.setValue(newValue);
            this.value = newValue;
            return true;
          } else {
            return false;
          }
        case MATRIX:
          MatrixValue mat = (MatrixValue) ((FinalOperator) newValue).getValue();
          if(mat.getMatrix().getElementType().getName().equals(currentVar.getType())) {
        //    currentVar.setValue(newValue);
            this.value = newValue;
            return true;
          } else {
           return false;
          }
        case CALL:
          CallInstruction call = (CallInstruction) ((FinalOperator) newValue).getValue();
          if(FunctionDeclaration.getImports().get(call.getName()) == null) {
            return false;
          }
          if(!FunctionDeclaration.getImports().get(call.getName()).getType().getName().equals(currentVar.getType())){
            return false;
          }
       //   currentVar.setValue(newValue);
          return true;
      }
    } else {

      if(!value.getClass().getSimpleName().equals(currentVar.getType())){
        return false;
      }

    }
  //  currentVar.setValue(newValue);
    this.value = newValue;
    return true;
  }

    public void setType(TypeInstruction type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public void writeByteCode(TraceClassVisitor cw, GeneratorAdapter mg) {
        try{
            MyVariable var = MyVariable.getByName(name);
            if(value != null){
              if(var.type() == Types.ARRAY) {
                  var.setValue(((MyArray)((FinalOperator)value).getValue()).getValue());
              }
              else {
                  var.setValue(value);
              }
            }
            var.writeByteCode(cw, mg);
        }
        //At this point, types were already checked, so its safe to asume the exception wont be thrown
        catch(Exception e){
            e.printStackTrace();
        }
    }
    @Override
    public TypeInstruction getType() {
        return this.type;
    }
}
