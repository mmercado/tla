import org.objectweb.asm.commons.GeneratorAdapter;
import org.objectweb.asm.util.TraceClassVisitor;

import static org.objectweb.asm.Opcodes.*;
import static org.objectweb.asm.Opcodes.INVOKESTATIC;

public class ReturnInstruction implements Instructions {
  private String functionName;
  private Types typeReturn;
  private Object value;

  public ReturnInstruction(String functionName, Types typeReturn, Object value) throws Exception {
    this.functionName = functionName;
    this.typeReturn = typeReturn;
    this.value = value;
    if(typeReturn != Types.VOID){
      if(!check(typeReturn, value)) {
        throw new Exception("Error Return Type.");
      }
    }
  }

  private boolean check(Types typeReturn, Object value) {
      if(typeReturn.getName().equals(value.getClass().getSimpleName())) {
        return true;
      } else {
        return false;
      }
  }

  public String getFunctionName() {
    return functionName;
  }

  public void setFunctionName(String functionName) {
    this.functionName = functionName;
  }

  public Types getTypeReturn() {
    return typeReturn;
  }

  public void setTypeReturn(Types typeReturn) {
    this.typeReturn = typeReturn;
  }

  public Object getValue() {
    return value;
  }

  public void setValue(Object value) {
    this.value = value;
  }

  @Override
  public TypeInstruction getType() {
    return TypeInstruction.RETURN;
  }

  @Override
  public void writeByteCode(TraceClassVisitor cw, GeneratorAdapter mg) {
      switch(typeReturn) {
        case INTEGEREXPRESION:
          mg.visitIntInsn(BIPUSH, (Integer)((FinalOperator)((IntegerExpresion)value).result()).getValue());
          mg.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
          mg.visitInsn(ARETURN);
          break;

        case DOUBLEEXPRESION:
          mg.visitLdcInsn((Double)((FinalOperator)((DoubleExpresion)value).result()).getValue());
          mg.visitMethodInsn(INVOKESTATIC, "java/lang/Double", "valueOf", "(D)Ljava/lang/Double;", false);
          mg.visitInsn(ARETURN);
          break;

        case STRING:
          String s = (String)value;
          mg.visitLdcInsn(s.substring(1, s.length()-1));
          mg.visitInsn(ARETURN);
          break;

        case BOOLEANEXPRESION:
          Boolean val = (Boolean)((FinalOperator)((BooleanExpresion)value).result()).getValue();
          int op = val.equals(true) ? ICONST_1 : ICONST_0;
          mg.visitInsn(op);
          mg.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;", false);
          mg.visitInsn(ARETURN);
          break;
        case VOID:
          mg.visitInsn(RETURN);
          break;

      }

  }


}
