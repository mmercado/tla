import org.objectweb.asm.commons.GeneratorAdapter;
import org.objectweb.asm.util.TraceClassVisitor;

import static org.objectweb.asm.Opcodes.*;
import static org.objectweb.asm.Opcodes.ASTORE;

public class DoubleExpresion implements Expression{
  private DoubleExpresion op1;
  private DoubleExpresion op2;
  private Operations operation;

  public DoubleExpresion(DoubleExpresion op1, DoubleExpresion op2, Operations operation) {
    this.op1 = op1;
    this.op2 = op2;
    this.operation = operation;
  }



  private FinalOperator value =  null;

  public DoubleExpresion(FinalOperator value) {
    this.value = value;
    this.operation = Operations.FINAL;
  }
  @Override
  public Object result() {
    if(value != null) {
      return this.getValue();
    }
    FinalOperator v1 = (FinalOperator) op1.result();
    FinalOperator v2 = (FinalOperator) op2.result();
    if(v1 != null && v2 != null) {
      if((v1.getValue() instanceof Double)&&(v2.getValue() instanceof Double)) {
        switch (operation) {
          case PLUS:
            this.setValue(new FinalOperator(FinalOperators.INTEGER, new Double(((Double) v1.getValue()).doubleValue() + ((Double) v2.getValue()).doubleValue())));
            return value;
          case MINUS:
            this.setValue(new FinalOperator(FinalOperators.INTEGER, new Double(((Double) v1.getValue()).doubleValue() - ((Double) v2.getValue()).doubleValue())));
            return value;
          case TIMES:
            this.setValue(new FinalOperator(FinalOperators.INTEGER, new Double(((Double) v1.getValue()).doubleValue() * ((Double) v2.getValue()).doubleValue())));
            return value;
          case DIV:
            this.setValue(new FinalOperator(FinalOperators.INTEGER, new Double(((Double) v1.getValue()).doubleValue() / ((Double) v2.getValue()).doubleValue())));
            return value;
        }
      }
    }
    return null;
  }

  public DoubleExpresion getOp1() {
    return op1;
  }

  public void setOp1(DoubleExpresion op1) {
    this.op1 = op1;
  }

  public DoubleExpresion getOp2() {
    return op2;
  }

  public void setOp2(DoubleExpresion op2) {
    this.op2 = op2;
  }

  public Operations getOperation() {
    return operation;
  }

  public void setOperation(Operations operation) {
    this.operation = operation;
  }

  public FinalOperator getValue() {
    return value;
  }

  public void setValue(FinalOperator value) {
    this.value = value;
  }

  @Override
  public void writeByteCode(TraceClassVisitor cw, GeneratorAdapter mg) {
      if(this.result() != null){
          FinalOperator f = (FinalOperator) result();
          f.writeByteCode(cw, mg);
      }else{
          op1.writeByteCode(cw, mg);
          op2.writeByteCode(cw, mg);

          switch (operation) {
              case PLUS:
                  mg.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Double", "doubleValue", "()D", false);
                  mg.swap();
                  mg.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Double", "doubleValue", "()D", false);
                  mg.visitInsn(DADD);
                  mg.visitMethodInsn(INVOKESTATIC, "java/lang/Double", "valueOf", "(D)Ljava/lang/Double;", false);
                  break;
              case MINUS:
                  mg.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Double", "doubleValue", "()D", false);
                  mg.swap();
                  mg.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Double", "doubleValue", "()D", false);
                  mg.visitInsn(DSUB);
                  mg.visitMethodInsn(INVOKESTATIC, "java/lang/Double", "valueOf", "(D)Ljava/lang/Double;", false);
                  break;
              case TIMES:
                  mg.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Double", "doubleValue", "()D", false);
                  mg.swap();
                  mg.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Double", "doubleValue", "()D", false);
                  mg.visitInsn(DMUL);
                  mg.visitMethodInsn(INVOKESTATIC, "java/lang/Double", "valueOf", "(D)Ljava/lang/Double;", false);
                  break;
              case DIV:
                  mg.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Double", "doubleValue", "()D", false);
                  mg.swap();
                  mg.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Double", "doubleValue", "()D", false);
                  mg.visitInsn(DDIV);
                  mg.visitMethodInsn(INVOKESTATIC, "java/lang/Double", "valueOf", "(D)Ljava/lang/Double;", false);
                  break;
//              case FINAL:
//                  break;
//              case AND:
//                  break;
//              case OR:
//                  break;
//              case NOT:
//                  break;
//              case G:
//                  break;
//              case GE:
//                  break;
//              case L:
//                  break;
//              case LE:
//                  break;
//              case EQUALS:
//                  break;
//              case NOTEQUALS:
//                  break;
//              case MODULE:
//                  break;
          }
      }
  }

}
