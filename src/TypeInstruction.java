
public enum TypeInstruction {

  ASSIGNMENT ("Assignment"), CONDITIONAL ("If"), CICLE ("While"), CALL("Call"),  ASSIGNMENTARRAY ("AssignmentArray"), ASSIGNMENTMATRIX ("AssignmentMatrix"), RETURN ("Return"),
  READINT("ReadInt"), READLINE("READLINE");
  String name;
  TypeInstruction(String string) {
    this.name = string;
  }
}
