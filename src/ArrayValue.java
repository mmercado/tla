
public class ArrayValue {
  private MyArray array;
  private IntegerExpresion index;

  public ArrayValue(MyArray array, IntegerExpresion index) {
    this.array = array;
    this.index = index;
  }

  public MyArray getArray() {
    return array;
  }

  public void setArray(MyArray array) {
    this.array = array;
  }

  public IntegerExpresion getIndex() {
    return index;
  }

  public void setIndex(IntegerExpresion index) {
    this.index = index;
  }
}
