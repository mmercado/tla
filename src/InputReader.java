
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.commons.GeneratorAdapter;
import org.objectweb.asm.*;
import org.objectweb.asm.commons.Method;
import org.objectweb.asm.util.TraceClassVisitor;


import java.io.InputStream;
import java.util.Scanner;

import static org.objectweb.asm.Opcodes.*;


public class InputReader {
  Scanner scanner;

  public InputReader() {
    scanner = new Scanner(System.in);
  }

    public void writeByteCode(TraceClassVisitor cw, GeneratorAdapter mg) {

//        mg.newInstance(Type.getType(Scanner.class));
//        mg.dup();
//        mg.getStatic(Type.getType(System.class), "in", Type.getType(InputStream.class));
//        Type[] args = {Type.getType(InputStream.class)};
//        Method m= new Method("<init>", Type.VOID_TYPE, args)
//        //Method m = Method.getMethod("void <init> (Ljava/io/InputStream;)");
//        mg.invokeConstructor(Type.getType(Scanner.class), m);
//        mg.storeLocal(1, Type.getType(Scanner.class));

        mg.visitTypeInsn(NEW, "java/util/Scanner");
        mg.visitInsn(DUP);
        mg.visitFieldInsn(GETSTATIC, "java/lang/System", "in", "Ljava/io/InputStream;");
        mg.visitMethodInsn(INVOKESPECIAL, "java/util/Scanner", "<init>", "(Ljava/io/InputStream;)V", false);
        mg.visitVarInsn(ASTORE, 1);

    }
}
