import org.objectweb.asm.Type;

public class Args {
  private String name;
  private Types type;

  public Args(String name, Types type) {
    this.name = name;
    this.type = type;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Args)) return false;

    Args args = (Args) o;

    if (getName() != null ? !getName().equals(args.getName()) : args.getName() != null) return false;
    return getType() == args.getType();

  }

  @Override
  public int hashCode() {
    int result = getName() != null ? getName().hashCode() : 0;
    result = 31 * result + (getType() != null ? getType().hashCode() : 0);
    return result;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Types getType() {
    return type;
  }

  public void setType(Types type) {
    this.type = type;
  }

  
}
