import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.commons.GeneratorAdapter;
import org.objectweb.asm.util.TraceClassVisitor;

import static org.objectweb.asm.Opcodes.*;
import static org.objectweb.asm.Opcodes.ASTORE;
import static org.objectweb.asm.Opcodes.INVOKESTATIC;

public abstract class  MyVariable {

  private Types type = DeclarationBlock.getCurrentType();
  private String name;
  private int index;
  protected boolean initialized;

  public static MyVariable getByName(String name) throws Exception {
    for (MyVariable mv : Program.getFunctionProperties().get(Program.getCurrentFunction()).getVariables()) {
      if (mv.getName().equals(name)) {
        return mv;
      }
    }
  for(Args a:FunctionDeclaration.getImports().get(Program.getCurrentFunction()).getArgs())
  {
    if (a.getName().equals(name)) {
      switch (a.getType()) {
        case INTARRAY:
          MyArray newArray = new MyArray(a.getName(), null);
          newArray.setType(Types.ARRAY);
          newArray.setElemTypes(Types.INTEGEREXPRESION);
          return newArray;
        case DOUBLEARRAY:
          MyArray newArray1 = new MyArray(a.getName(), null);
          newArray1.setType(Types.ARRAY);
          newArray1.setElemTypes(Types.DOUBLEEXPRESION);
          return newArray1;
        case BOOLEANARRAY:
          MyArray newArray2 = new MyArray(a.getName(), null);
          newArray2.setType(Types.ARRAY);
          newArray2.setElemTypes(Types.BOOLEANEXPRESION);
          return newArray2;
        case STRINGARRAY:
          MyArray newArray3 = new MyArray(a.getName(), null);
          newArray3.setType(Types.ARRAY);
          newArray3.setElemTypes(Types.STRING);
          return newArray3;
        case INTEGEREXPRESION:
          MyVariable var = new Assingment(a.getName(), null);
          var.setInitialized(true);
          var.setType(Types.INTEGEREXPRESION);
          return var;
        case BOOLEANEXPRESION:
          MyVariable var1 = new Assingment(a.getName(), null);
          var1.setInitialized(true);
          var1.setType(Types.BOOLEANEXPRESION);
          return var1;
        case DOUBLEEXPRESION:
          MyVariable var2 = new Assingment(a.getName(), null);
          var2.setInitialized(true);
          var2.setType(Types.DOUBLEEXPRESION);
          return var2;
        case STRING:
          MyVariable var3 = new Assingment(a.getName(), null);
          var3.setInitialized(true);
          var3.setType(Types.STRING);
          return var3;
      }

    }
  }

  throw new

  Exception("Variable "+name+" does not exist.");

}

    public MyVariable(String name) throws Exception {
        this.name = name;

      for(MyVariable x : Program.getFunctionProperties().get(Program.getCurrentFunction()).getVariables()) {
          if(x.getName().equals(name)){
            throw new Exception("Variable "+ name +" exist.");
          }
        }
      this.initialized = true;
    }

  public void setType(Types type) {
    this.type = type;
  }

  public String getType() {return this.type.getName();}

    public abstract void setValue(Object value) throws Exception;

    public Types getTypes() {return this.type;}

    public abstract Object getValue();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }


    public abstract Types type();

    public void writeByteCode(TraceClassVisitor cw, GeneratorAdapter mg) {

        if(this.getValue() == null)   //EmptyVariable
            return;

        switch (type()) {

            case INTEGEREXPRESION:
                IntegerExpresion i = (IntegerExpresion)getValue();
                i.writeByteCode(cw, mg);
                mg.visitVarInsn(ASTORE, getIndex());

                break;

            case DOUBLEEXPRESION:

                DoubleExpresion d = (DoubleExpresion)getValue();
                d.writeByteCode(cw, mg);
                mg.visitVarInsn(ASTORE, getIndex());

                break;

            case STRING:
                FinalOperator str = (FinalOperator) getValue();
                str.writeByteCode(cw, mg);
                mg.visitVarInsn(ASTORE, getIndex());
                break;

            case ARRAY:
                MyArray arr = (MyArray) this;

                //Creo el array
                String type = arr.getElemTypes().getClasz().getSimpleName();
                int size = arr.getDim();
                mg.visitIntInsn(BIPUSH, size); //SIZE del array
                mg.visitTypeInsn(ANEWARRAY, "java/lang/" + type); //CLASE del array
                mg.visitVarInsn(ASTORE, getIndex()); //guardo el array en el stack de variables

                for(int j=0; j<size; j++) {
                  IntegerExpresion ie = new IntegerExpresion(new FinalOperator(FinalOperators.INTEGER,Integer.valueOf(j)));
                  if(arr.getElem(ie) != null) {
                    mg.visitVarInsn(ALOAD, getIndex());  //cargo la referencia al array
                    mg.visitIntInsn(BIPUSH, j);
                    Program.pushValue(mg, arr.getElem(ie), arr.getElemTypes());
                    mg.visitInsn(AASTORE);
                  }
                }
                break;
            case MATRIX:
                break;

            case BOOLEANEXPRESION:
                //TEMPLATE
                BooleanExpresion b = (BooleanExpresion) getValue();
                b.writeByteCode(cw, mg);
                mg.visitVarInsn(ASTORE, getIndex());

                break;
        }
    }

    public void loadVariable(TraceClassVisitor cw, GeneratorAdapter mg){
        if(this.getValue() == null)   //EmptyVariable
            return;

        switch (type) {

            case INTEGEREXPRESION:
                IntegerExpresion i = (IntegerExpresion) getValue();
                i.writeByteCode(cw, mg);
                break;

            case DOUBLEEXPRESION:
                DoubleExpresion d = (DoubleExpresion) getValue();
                d.writeByteCode(cw, mg);
                break;

            case STRING:
                FinalOperator str = (FinalOperator) getValue();
                str.writeByteCode(cw, mg);
                break;

            case BOOLEANEXPRESION:
                BooleanExpresion b = (BooleanExpresion) getValue();
                b.writeByteCode(cw, mg);
                break;
            case ARRAY:
                break;
            case MATRIX:
                break;
        }
    }

    public boolean isInitialized() {
        return initialized;
    }

    public void setInitialized(boolean initialized) {
        this.initialized = initialized;
    }
}

