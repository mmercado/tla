import org.objectweb.asm.commons.GeneratorAdapter;
import org.objectweb.asm.util.TraceClassVisitor;

import static org.objectweb.asm.Opcodes.*;

public class ReadInt implements Instructions{
  MyVariable variable;

  public ReadInt(MyVariable a) {
    this.variable = a;
  }

  public Integer readInt() {
    return 0;
  }

  @Override
  public TypeInstruction getType() {
    return null;
  }

  @Override
  public void writeByteCode(TraceClassVisitor cw, GeneratorAdapter mg) {
    mg.visitFieldInsn(GETSTATIC, "Example", "scanner", "Ljava/util/Scanner;");
    mg.visitMethodInsn(INVOKEVIRTUAL, "java/util/Scanner", "nextInt", "()I", false);
    //mg.visitInsn(POP);
    mg.visitVarInsn(ASTORE,variable.getIndex());

  }
}
