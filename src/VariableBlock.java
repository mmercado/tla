import org.objectweb.asm.commons.GeneratorAdapter;
import org.objectweb.asm.util.TraceClassVisitor;

import java.util.ArrayList;
import java.util.List;

import static org.objectweb.asm.Opcodes.*;

public class VariableBlock {
    Types type;
    List<MyVariable> variableList;


    public VariableBlock(Types type, List<MyVariable> variableList) {
        this.type = type;
        this.variableList = variableList;
    }

    public Types getType() {
        return type;
    }

    public void setType(Types type) {
        this.type = type;
    }

    public List<MyVariable> getVariableList() {
        return variableList;
    }

    public void setVariableList(List<MyVariable> variableList) {
        this.variableList = variableList;
    }

    public void writeByteCode(TraceClassVisitor cw, GeneratorAdapter mg) {
        //type.writeByteCode(cw, mg, type);

        for(MyVariable variable: variableList){

            if(variable.getValue() == null || !variable.isInitialized()){
                variable.setInitialized(true);
            }else{
                variable.writeByteCode(cw, mg);
            }

        }
    }

    public int sortVariableList(int i) {
        List<MyVariable> sortedList = new ArrayList<>();

        //Reverse the list and add to each variable the corresponding index for the local variables stack
        //(The list is reversed because elements were originally visited backwards)
        for(int j=variableList.size() -1; j>=0; j--){
            variableList.get(j).setIndex(i++);
            sortedList.add(variableList.get(j));
        }
        this.variableList = sortedList;

        return i;
    }

}
