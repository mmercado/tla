
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.commons.GeneratorAdapter;
import org.objectweb.asm.commons.Method;
import org.objectweb.asm.util.TraceClassVisitor;

import java.util.List;

import static org.objectweb.asm.Opcodes.*;

public class Main {
    private List<Args> args;
    private Block block;

    public Main(List<Args> args, Block block) {
        this.args = args;
        this.block = block;
    }

    public List<Args> getArgs() {
        return args;
    }

    public void setArgs(List<Args> args) {
        this.args = args;
    }

    public Block getBlock() {
        return block;
    }

    public void setBlock(Block block) {
        this.block = block;
    }

    public void writeByteCode(TraceClassVisitor cw) {
        // creates a GeneratorAdapter for the 'main' method
        Method m = Method.getMethod("void main (String[])");
        GeneratorAdapter mg = new GeneratorAdapter(ACC_PUBLIC + ACC_STATIC, m, null, null, cw);

        mg.visitTypeInsn(NEW, "java/util/Scanner");
        mg.visitInsn(DUP);
        mg.visitFieldInsn(GETSTATIC, "java/lang/System", "in", "Ljava/io/InputStream;");
        mg.visitMethodInsn(INVOKESPECIAL, "java/util/Scanner", "<init>", "(Ljava/io/InputStream;)V", false);
        mg.visitFieldInsn(PUTSTATIC, "Example", "scanner", "Ljava/util/Scanner;");



//    for(Args arg: args){
//      arg.writeByteCode(cw, mg);
//    }
        block.writeByteCode(cw, mg);

        mg.visitMaxs(1,1);
        mg.endMethod();
        return;

    }

}
