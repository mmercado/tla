import org.objectweb.asm.Label;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.commons.GeneratorAdapter;
import org.objectweb.asm.util.TraceClassVisitor;

import static org.objectweb.asm.Opcodes.*;
import static org.objectweb.asm.Opcodes.GOTO;
import static org.objectweb.asm.Opcodes.ICONST_0;

public class BooleanExpresion implements Expression{

  private BooleanExpresion op1;
  private BooleanExpresion op2;
  private Operations operation;

  public BooleanExpresion(BooleanExpresion op1, BooleanExpresion op2, Operations operation) {
    this.op1 = op1;
    this.op2 = op2;
    this.operation = operation;
  }

  private IntegerExpresion intOP1;
  private IntegerExpresion intOP2;

  public BooleanExpresion(Operations operation, IntegerExpresion intOP1, IntegerExpresion intOP2) {
    this.operation = operation;
    this.intOP1 = intOP1;
    this.intOP2 = intOP2;
  }

  private FinalOperator value =  null;

  public BooleanExpresion(FinalOperator value) {
    this.value = value;
    this.operation = Operations.FINAL;
  }

  @Override
  public Object result() {
    if(value != null) {
      return this.value;
    }
    if(op1 != null){
        FinalOperator v1 = (FinalOperator) op1.result();
        if(v1 != null && this.getOperation().getName().equals(Operations.NOT.getName())) {
            if(v1.getValue() instanceof Boolean) {
                this.setValue(new FinalOperator(FinalOperators.BOOLEAN,new Boolean(!(((Boolean) v1.getValue()).booleanValue()))));
                return this.value.getValue();
            }
        }
    }
    if(op1 != null && op2 != null) {
      FinalOperator v1 = (FinalOperator) op1.result();
      FinalOperator v2 = (FinalOperator) op2.result();
      if (v1 != null && v2 != null) {
        if ((v1.getValue() instanceof Boolean) && (v2.getValue() instanceof Boolean)) {
          switch (operation) {
            case AND:
              this.setValue(new FinalOperator(FinalOperators.BOOLEAN, new Boolean(((Boolean) v1.getValue()).booleanValue() && ((Boolean) v2.getValue()).booleanValue())));
              return this.value;
            case OR:
              this.setValue(new FinalOperator(FinalOperators.BOOLEAN, new Boolean(((Boolean) v1.getValue()).booleanValue() || ((Boolean) v2.getValue()).booleanValue())));
              return this.value;

          }
        }
      }
      if (intOP1 != null && intOP2 != null) {
        v1 = (FinalOperator) intOP1.result();
        v2 = (FinalOperator) intOP2.result();
          if (v1 != null && v2 != null) {
              if ((v1.getValue() instanceof Integer) && (v2.getValue() instanceof Integer)) {
                  switch (operation) {
                      case G:
                          this.setValue(new FinalOperator(FinalOperators.BOOLEAN, new Boolean(((Integer) v1.getValue()).intValue() > ((Integer) v2.getValue()).intValue())));
                          return this.value;
                      case GE:
                          this.setValue(new FinalOperator(FinalOperators.BOOLEAN, new Boolean(((Integer) v1.getValue()).intValue() >= ((Integer) v2.getValue()).intValue())));
                          return this.value;
                      case L:
                          this.setValue(new FinalOperator(FinalOperators.BOOLEAN, new Boolean(((Integer) v1.getValue()).intValue() < ((Integer) v2.getValue()).intValue())));
                          return this.value;
                      case LE:
                          this.setValue(new FinalOperator(FinalOperators.BOOLEAN, new Boolean(((Integer) v1.getValue()).intValue() <= ((Integer) v2.getValue()).intValue())));
                          return this.value;
                      case EQUALS:
                          this.setValue(new FinalOperator(FinalOperators.BOOLEAN, new Boolean(((Integer) v1.getValue()).intValue() == ((Integer) v2.getValue()).intValue())));
                          return this.value;
                      case NOTEQUALS:
                          this.setValue(new FinalOperator(FinalOperators.BOOLEAN, new Boolean(((Integer) v1.getValue()).intValue() != ((Integer) v2.getValue()).intValue())));
                          return this.value;

                  }
              }
          }
      }
    }
    return null;
  }

  public BooleanExpresion getOp1() {
    return op1;
  }

  public void setOp1(BooleanExpresion op1) {
    this.op1 = op1;
  }

  public BooleanExpresion getOp2() {
    return op2;
  }

  public void setOp2(BooleanExpresion op2) {
    this.op2 = op2;
  }

  public Operations getOperation() {
    return operation;
  }

  public void setOperation(Operations operation) {
    this.operation = operation;
  }

  public FinalOperator getValue() {
    return value;
  }

  public void setValue(FinalOperator value) {
    this.value = value;
  }

    @Override
    public void writeByteCode(TraceClassVisitor cw, GeneratorAdapter mg) {
        if (this.result() != null) {
            FinalOperator f = (FinalOperator) result();
            f.writeByteCode(cw, mg);
        } else {

            switch (operation) {
                case AND:
                    op1.writeByteCode(cw, mg);
                    op2.writeByteCode(cw, mg);
                    mg.swap();
                    mg.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Boolean", "booleanValue", "()Z", false);
                    mg.swap();
                    mg.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Boolean", "booleanValue", "()Z", false);
                    Label l13 = new Label();
                    mg.visitJumpInsn(IFEQ, l13);
                    mg.visitJumpInsn(IFEQ, l13);
                    mg.visitInsn(ICONST_1);
                    Label l14 = new Label();
                    mg.visitJumpInsn(GOTO, l14);
                    mg.visitLabel(l13);
                    mg.visitInsn(ICONST_0);
                    mg.visitLabel(l14);
                    mg.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;", false);
                    //mg.visitVarInsn(ASTORE, 2);
                    break;
                case OR:
                    op1.writeByteCode(cw, mg);
                    op2.writeByteCode(cw, mg);
                    mg.swap();
                    mg.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Boolean", "booleanValue", "()Z", false);
                    mg.swap();
                    mg.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Boolean", "booleanValue", "()Z", false);

                    Label l15 = new Label();
                    mg.visitJumpInsn(IFNE, l15);
                    Label l16 = new Label();
                    mg.visitJumpInsn(IFEQ, l16);
                    mg.visitLabel(l15);
                    mg.visitInsn(ICONST_1);
                    Label l17 = new Label();
                    mg.visitJumpInsn(GOTO, l17);
                    mg.visitLabel(l16);
                    mg.visitInsn(ICONST_0);
                    mg.visitLabel(l17);
                    mg.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;", false);

                    break;
                case NOT:
                    op1.writeByteCode(cw, mg);
                    mg.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Boolean", "booleanValue", "()Z", false);
                    Label l18 = new Label();
                    mg.visitJumpInsn(IFNE, l18);
                    mg.visitInsn(ICONST_1);
                    Label l19 = new Label();
                    mg.visitJumpInsn(GOTO, l19);
                    mg.visitLabel(l18);
                    mg.visitInsn(ICONST_0);
                    mg.visitLabel(l19);
                    mg.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;", false);
                    break;
                case G:
                    intOP1.writeByteCode(cw, mg);
                    intOP2.writeByteCode(cw, mg);
                    mg.swap();
                    mg.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Integer", "intValue", "()I", false);
                    mg.swap();
                    mg.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Integer", "intValue", "()I", false);
                    Label l1 = new Label();
                    mg.visitJumpInsn(IF_ICMPLE, l1);
                    mg.visitInsn(ICONST_1);
                    Label l2 = new Label();
                    mg.visitJumpInsn(GOTO, l2);
                    mg.visitLabel(l1);
                    mg.visitInsn(ICONST_0);
                    mg.visitLabel(l2);
                    mg.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;", false);
                    break;
                case GE:
                    intOP1.writeByteCode(cw, mg);
                    intOP2.writeByteCode(cw, mg);
                    mg.swap();
                    mg.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Integer", "intValue", "()I", false);
                    mg.swap();
                    mg.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Integer", "intValue", "()I", false);
                    Label l3 = new Label();
                    mg.visitJumpInsn(IF_ICMPLT, l3);
                    mg.visitInsn(ICONST_1);
                    Label l4 = new Label();
                    mg.visitJumpInsn(GOTO, l4);
                    mg.visitLabel(l3);
                    mg.visitInsn(ICONST_0);
                    mg.visitLabel(l4);
                    mg.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;", false);
                    break;
                case L:
                    intOP1.writeByteCode(cw, mg);
                    intOP2.writeByteCode(cw, mg);
                    mg.swap();
                    mg.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Integer", "intValue", "()I", false);
                    mg.swap();
                    mg.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Integer", "intValue", "()I", false);
                    Label l5 = new Label();
                    mg.visitJumpInsn(IF_ICMPGE, l5);
                    mg.visitInsn(ICONST_1);
                    Label l6 = new Label();
                    mg.visitJumpInsn(GOTO, l6);
                    mg.visitLabel(l5);
                    mg.visitInsn(ICONST_0);
                    mg.visitLabel(l6);
                    mg.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;", false);
                    break;
                case LE:
                    intOP1.writeByteCode(cw, mg);
                    intOP2.writeByteCode(cw, mg);
                    mg.swap();
                    mg.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Integer", "intValue", "()I", false);
                    mg.swap();
                    mg.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Integer", "intValue", "()I", false);
                    Label l7 = new Label();
                    mg.visitJumpInsn(IF_ICMPGT, l7);
                    mg.visitInsn(ICONST_1);
                    Label l8 = new Label();
                    mg.visitJumpInsn(GOTO, l8);
                    mg.visitLabel(l7);
                    mg.visitInsn(ICONST_0);
                    mg.visitLabel(l8);
                    mg.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;", false);
                    break;
                case EQUALS:
                    intOP1.writeByteCode(cw, mg);
                    intOP2.writeByteCode(cw, mg);
                    mg.swap();
                    mg.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Integer", "intValue", "()I", false);
                    mg.swap();
                    mg.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Integer", "intValue", "()I", false);
                    Label l9 = new Label();
                    mg.visitJumpInsn(IF_ACMPNE, l9);
                    mg.visitInsn(ICONST_1);
                    Label l10 = new Label();
                    mg.visitJumpInsn(GOTO, l10);
                    mg.visitLabel(l9);
                    mg.visitInsn(ICONST_0);
                    mg.visitLabel(l10);
                    mg.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;", false);
                    break;
                case NOTEQUALS:
                    intOP1.writeByteCode(cw, mg);
                    intOP2.writeByteCode(cw, mg);
                    mg.swap();
                    mg.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Integer", "intValue", "()I", false);
                    mg.swap();
                    mg.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Integer", "intValue", "()I", false);
                    Label l11 = new Label();
                    mg.visitJumpInsn(IF_ACMPEQ, l11);
                    mg.visitInsn(ICONST_1);
                    Label l12 = new Label();
                    mg.visitJumpInsn(GOTO, l12);
                    mg.visitLabel(l11);
                    mg.visitInsn(ICONST_0);
                    mg.visitLabel(l12);
                    mg.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;", false);
                    break;
                //Ignore
//                case MODULE:
//                    break;
            }

        }
    }
}

