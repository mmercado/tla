import java.util.HashMap;
import java.util.List;

public class FunctionDeclaration {

  private static HashMap<String,FunctionDeclaration> imports = new HashMap<>();
  private String name;
  private Types type;
  private List<Args> args;

  public FunctionDeclaration(String name, Types type, List<Args> args) {
    this.name = name;
    this.type = type;
    this.args = args;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof FunctionDeclaration)) return false;

    FunctionDeclaration that = (FunctionDeclaration) o;

    if (!getName().equals(that.getName())) return false;
    if (getType() != that.getType()) return false;
    return getArgs() != null ? getArgs().equals(that.getArgs()) : that.getArgs() == null;

  }

  @Override
  public int hashCode() {
    int result = getName().hashCode();
    result = 31 * result + getType().hashCode();
    result = 31 * result + (getArgs() != null ? getArgs().hashCode() : 0);
    return result;
  }

  public static HashMap<String, FunctionDeclaration> getImports() {
    return imports;
  }

  public static void setImports(HashMap<String, FunctionDeclaration> imports) {
    FunctionDeclaration.imports = imports;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Types getType() {
    return type;
  }

  public void setType(Types type) {
    this.type = type;
  }

  public List<Args> getArgs() {
    return args;
  }

  public void setArgs(List<Args> args) {
    this.args = args;
  }
}
