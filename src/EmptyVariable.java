
public class EmptyVariable extends MyVariable{
  private Object value = null;

  public EmptyVariable(String name) throws Exception {
    super(name);
  }

  @Override
  public void setValue(Object value) throws Exception {
    this.value = value;
  }

  @Override
  public Object getValue() {
    return this.value;
  }

  public Types type(){
    return Types.EMPTYVARIABLE;
  }

}
