import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.commons.GeneratorAdapter;
import org.objectweb.asm.util.TraceClassVisitor;

import static org.objectweb.asm.Opcodes.*;

public class CicleInstruction implements Instructions{
  BooleanExpresion condition;
  CodeBlock codeBlock;
  TypeInstruction typeInstruction;

  public CicleInstruction(BooleanExpresion condition, CodeBlock codeBlock) {
    this.condition = condition;
    this.codeBlock = codeBlock;
    this.typeInstruction = TypeInstruction.CICLE;
  }

  public BooleanExpresion getCondition() {
    return condition;
  }

  public void setCondition(BooleanExpresion condition) {
    this.condition = condition;
  }

  public CodeBlock getCodeBlock() {
    return codeBlock;
  }

  public void setCodeBlock(CodeBlock codeBlock) {
    this.codeBlock = codeBlock;
  }

  @Override
  public TypeInstruction getType() {
    return this.typeInstruction;
  }

  @Override
  public void writeByteCode(TraceClassVisitor cw, GeneratorAdapter mg) {
      Label l1 = new Label();
      mg.visitLabel(l1);

      //Check Condition
//      mg.visitVarInsn(ILOAD, 0);
//      mg.visitIntInsn(BIPUSH, 10);
      condition.writeByteCode(cw, mg);
      mg.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Boolean", "booleanValue", "()Z", false);
      Label l2 = new Label();
      mg.visitJumpInsn(IF_ICMPGE, l2);

      //Do stuff
//      Label l3 = new Label();
//      mg.visitLabel(l3);
//      mg.visitLineNumber(48, l3);
//      mg.visitIincInsn(0, 1);
      codeBlock.writeByteCode(cw, mg);

      //Loop again
      mg.visitJumpInsn(GOTO, l1);

      mg.visitLabel(l2);
  }
}
