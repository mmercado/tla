import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.commons.GeneratorAdapter;
import org.objectweb.asm.util.TraceClassVisitor;

import static org.objectweb.asm.Opcodes.*;

public class FinalOperator {
  private FinalOperators type;
  private Object value;

  public FinalOperator(FinalOperators type, Object value) {
    this.type = type;
    this.value = value;
  }

  public FinalOperators getType() {
    return type;
  }

  public Object getValue() {
    return value;
  }

  public void writeByteCode(TraceClassVisitor cw, GeneratorAdapter mg){
      switch (type) {
          case INTEGER:
              //push(value)
              Integer i = (Integer)this.value;
              mg.visitIntInsn(BIPUSH, i.intValue());
              mg.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false);
              break;

          case CALL:
              ((CallInstruction)this.value).writeByteCode(cw, mg);
              break;

          case VARIABLE:
              MyVariable var = (MyVariable)this.value;

            mg.visitVarInsn(ALOAD, var.getIndex());

              break;

          case MATRIX:
              //MatrixValue.writeByteCode()
              break;

          case ARRAY:
              //1.Tomo el array del stack de variables
              ArrayValue arr = (ArrayValue) this.value;
              int index = arr.getArray().getIndex();

              //2.Lo pusheo en el operand stack
              mg.visitVarInsn(ALOAD, index);

              //3.Pusheo el indice del array al que quiero acceder
              arr.getIndex().writeByteCode(cw, mg); //--> pega el codigo asm para resolver el indice. En el stack queda index

              //4.Tomo el elemento array[index]
              mg.visitInsn(IALOAD);

              break;

          case BOOLEAN:
              //push(value)
              //Boolean bool = (Boolean) this.value;
              if( ((Boolean) this.value) == true){
                  mg.visitInsn(ICONST_1);
              }else{
                  mg.visitInsn(ICONST_0);
              }
              mg.visitMethodInsn(INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;", false);
              break;

          case DOUBLE:
              //push(value)
              Double d = (Double)getValue();
              mg.visitLdcInsn(new Double(d.toString()));
              mg.visitMethodInsn(INVOKESTATIC, "java/lang/Double", "valueOf", "(D)Ljava/lang/Double;", false);
              break;

          case STRING:
              //push(value)
              String str = (String) this.value;

              str = str.substring(1, str.length()-1);
              mg.visitLdcInsn(str);
              break;
      }
  }
}
