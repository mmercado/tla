import org.objectweb.asm.commons.GeneratorAdapter;
import org.objectweb.asm.util.TraceClassVisitor;

import static org.objectweb.asm.Opcodes.ALOAD;
import static org.objectweb.asm.Opcodes.GETSTATIC;
import static org.objectweb.asm.Opcodes.INVOKEVIRTUAL;

public class PrintMsg implements Instructions{
  Object msg;

  public Object getMsg() {
    return msg;
  }

  public void setMsg(Object msg) {
    this.msg = msg;
  }

  public PrintMsg(Object msg) {
    this.msg = msg;
  }

  @Override
  public TypeInstruction getType() {
    return null;
  }

  @Override
  public void writeByteCode(TraceClassVisitor cw, GeneratorAdapter mg) {
    if(msg == null)
      return;
    mg.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
    //mg.visitVarInsn(ALOAD, 0);
    ((FinalOperator)msg).writeByteCode(cw, mg);

    //mg.visitLdcInsn(msg.substring(1, msg.length()-1));
    mg.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false);

  }
}
