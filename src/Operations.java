
public enum Operations {
  PLUS ("+"), MINUS ("-"), TIMES ("*"), DIV ("/"), FINAL("final"), AND("&&"), OR("||"), NOT("!"), G(">"), GE(">="), L("<"), LE("<="), EQUALS("=="), NOTEQUALS("!="), MODULE("%");

  Operations(String name) {
    this.name = name;
  }

  String name;

  public String getName() {
    return name;
  }
}
