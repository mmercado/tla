import java.util.ArrayList;
import java.util.List;

public class FunctionPropereties {
  List<MyVariable> variables;
  boolean hasReturn = false;
  Types type;

  public FunctionPropereties(Types type) {
    this.variables = new ArrayList<>();
    this.type = type;
  }

  public List<MyVariable> getVariables() {
    return variables;
  }

  public void setVariables(List<MyVariable> variables) {
    this.variables = variables;
  }

  public boolean isHasReturn() {
    return hasReturn;
  }

  public void setHasReturn(boolean hasReturn) {
    this.hasReturn = hasReturn;
  }

  public Types getType() {
    return type;
  }

  public void setType(Types type) {
    this.type = type;
  }
}
