

public class Assingment extends MyVariable{
  Object value;

  public Assingment(String name, Object newValue) throws Exception {
    super(name);
    if (newValue == null) {
      this.initialized = false;
      this.value = null;
    } else {
      if (newValue instanceof FinalOperator) {
        switch (((FinalOperator) newValue).getType()) {
          case VARIABLE:
            MyVariable var = (MyVariable) ((FinalOperator) newValue).getValue();
            if(!var.isInitialized()) {
              throw new Exception("Error variable "+ var.getName() +" in not initialized.");
            }
            if (!var.getType().equals(DeclarationBlock.getCurrentType().getName())) {
              throw new Exception("Assigment1: Type Mismatched, Expectedd Type: " + DeclarationBlock.getCurrentType().getName());
            }
            break;
          case ARRAY:
            ArrayValue arr = (ArrayValue) ((FinalOperator) newValue).getValue();
            if (!arr.getArray().getElemTypes().getName().equals(DeclarationBlock.getCurrentType().getName())) {
              throw new Exception("Assigment1: Type Mismatched, Expectedd Type: " + DeclarationBlock.getCurrentType().getName());
            }
          case MATRIX:
            MatrixValue mat = (MatrixValue) ((FinalOperator) newValue).getValue();
            if (!mat.getMatrix().getElementType().getName().equals(DeclarationBlock.getCurrentType().getName())) {
              throw new Exception("Assigment1: Type Mismatched, Expectedd Type: " + DeclarationBlock.getCurrentType().getName());
            }
            break;
          case CALL:
            CallInstruction call = (CallInstruction) ((FinalOperator) newValue).getValue();
            if (FunctionDeclaration.getImports().get(call.getName()) == null) {
              throw new Exception("Assigment1: Type Mismatched, Expected Type: " + DeclarationBlock.getCurrentType().getName());
            }
            if (!FunctionDeclaration.getImports().get(call.getName()).getType().equals(DeclarationBlock.getCurrentType())) {
              throw new Exception("Assigment1: Type Mismatched, Expected Type " + DeclarationBlock.getCurrentType().getName());
            }
            break;
        }
      } else {
        if (!newValue.getClass().getSimpleName().equals(DeclarationBlock.getCurrentType().getName())) {
          throw new Exception("Assigment1: Type Mismatched, Expected Type: " + DeclarationBlock.getCurrentType().getName());
        }

      }
      this.value = newValue;
    }
  }

  public Object getValue() {
    return value;
  }

  public void setValue(Object value) throws Exception {
    this.value = value;
  }

  public Types type(){
    return getTypes();
  }

  public void test(){
    Integer a = 5;
      a = 2;
  }

}
