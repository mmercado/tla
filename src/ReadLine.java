import org.objectweb.asm.commons.GeneratorAdapter;
import org.objectweb.asm.util.TraceClassVisitor;

import static org.objectweb.asm.Opcodes.*;

public class ReadLine implements Instructions{
  MyVariable var;

  public ReadLine(MyVariable var) {
    this.var = var;
  }

  public String readLine(){
    return "";
  }

  @Override
  public TypeInstruction getType() {
    return null;
  }

  @Override
  public void writeByteCode(TraceClassVisitor cw, GeneratorAdapter mg) {
    mg.visitFieldInsn(GETSTATIC, "Example", "scanner", "Ljava/util/Scanner;");
    mg.visitMethodInsn(INVOKEVIRTUAL, "java/util/Scanner", "next", "()Ljava/lang/String;", false);
    mg.visitVarInsn(ASTORE,var.getIndex());

    //mg.visitInsn(POP);
  }
}
