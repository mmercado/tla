
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.commons.GeneratorAdapter;
import org.objectweb.asm.util.TraceClassVisitor;

import java.util.List;

public class CodeBlock {

    List<Instructions> instructions;

    public CodeBlock(List<Instructions> instructions) {
        this.instructions = instructions;
    }

    public List<Instructions> getInstructions() {
        return instructions;
    }

    public void setInstructions(List<Instructions> instructions) {
        this.instructions = instructions;
    }


    public void writeByteCode(TraceClassVisitor cw, GeneratorAdapter mg) {

        for(Instructions instruction : instructions){
            instruction.writeByteCode(cw, mg);
        }

    }
}
