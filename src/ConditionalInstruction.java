import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.commons.GeneratorAdapter;
import org.objectweb.asm.util.TraceClassVisitor;

import static org.objectweb.asm.Opcodes.*;
import static org.objectweb.asm.Opcodes.GOTO;
import static org.objectweb.asm.Opcodes.ICONST_0;

public class ConditionalInstruction implements Instructions {
  BooleanExpresion condition;
  CodeBlock codeBlock;
  TypeInstruction typeInstruction;
  CodeBlock elseBlock;

  public ConditionalInstruction(BooleanExpresion condition, CodeBlock codeBlock) {
    this.condition = condition;
    this.codeBlock = codeBlock;
    this.elseBlock = null;
    this.typeInstruction = TypeInstruction.CONDITIONAL;
  }

  public ConditionalInstruction(BooleanExpresion condition, CodeBlock codeBlock, CodeBlock elseBlock) {
    this.condition = condition;
    this.codeBlock = codeBlock;
    this.elseBlock = elseBlock;
    this.typeInstruction = TypeInstruction.CONDITIONAL;
  }

  public BooleanExpresion getCondition() {
    return condition;
  }

  public void setCondition(BooleanExpresion condition) {
    this.condition = condition;
  }

  public CodeBlock getCodeBlock() {
    return codeBlock;
  }

  public void setCodeBlock(CodeBlock codeBlock) {
    this.codeBlock = codeBlock;
  }

  public CodeBlock getElseBlock() {
    return elseBlock;
  }

  public void setElseBlock(CodeBlock elseBlock) {
    this.elseBlock = elseBlock;
  }

  @Override
  public TypeInstruction getType() {
    return this.typeInstruction;
  }

  @Override
  public void writeByteCode(TraceClassVisitor cw, GeneratorAdapter mg) {

     condition.writeByteCode(cw, mg);
      Label l2 = new Label();
      mg.visitJumpInsn(IFEQ, l2);
      codeBlock.writeByteCode(cw, mg);
      Label l4 = new Label();
      mg.visitJumpInsn(GOTO, l4);
      mg.visitLabel(l2);
      if(elseBlock != null){
        elseBlock.writeByteCode(cw, mg);
      }
      mg.visitLabel(l4);


  }

}
